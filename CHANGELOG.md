## [1.1.4](https://gitlab.com/alexwine/aws-cron-generator/compare/v1.1.3...v1.1.4) (2021-08-23)


### Bug Fixes

* **types:** updated type location in package.json ([4270ae7](https://gitlab.com/alexwine/aws-cron-generator/commit/4270ae7f1a3def79ad8dd47f93ceb674da53a3c0))

## [1.1.3](https://gitlab.com/alexwine/aws-cron-generator/compare/v1.1.2...v1.1.3) (2021-08-23)


### Bug Fixes

* **package.json:** updated files to dist ([3b6e034](https://gitlab.com/alexwine/aws-cron-generator/commit/3b6e0349c803ee3a39788a7551df99bf244741d9))

## [1.1.2](https://gitlab.com/alexwine/aws-cron-generator/compare/v1.1.1...v1.1.2) (2021-08-23)


### Bug Fixes

* **ci:** fixed validation error on ci file ([2521efb](https://gitlab.com/alexwine/aws-cron-generator/commit/2521efb334111331842ad3306b423a98343ed370))
* **doc generation:** potential fix for wrong doc version being displayed ([ce0eba6](https://gitlab.com/alexwine/aws-cron-generator/commit/ce0eba68d953ba30207adc4a49b13ef0acb829b6))

## [1.1.1](https://gitlab.com/alexwine/aws-cron-generator/compare/v1.1.0...v1.1.1) (2021-08-23)


### Bug Fixes

* **package.json:** added files to package to ignore other files ([518a048](https://gitlab.com/alexwine/aws-cron-generator/commit/518a048fea75bce61ac0b12992c8239d57593fcb))

# [1.1.0](https://gitlab.com/alexwine/aws-cron-generator/compare/v1.0.1...v1.1.0) (2021-08-23)


### Bug Fixes

* **gitignore:** fixed incorrect file in gitignore ([b930daa](https://gitlab.com/alexwine/aws-cron-generator/commit/b930daaa0d8433bb7259b308539be27b9b6d46f0))


### Features

* **cront-time:** updated return functions for formatting to AWS standards ([16c231f](https://gitlab.com/alexwine/aws-cron-generator/commit/16c231f8e2b63654df82798aa461ed7660c3eece))

## [1.0.1](https://gitlab.com/alexwine/aws-cron-generator/compare/v1.0.0...v1.0.1) (2021-08-23)


### Bug Fixes

* **standard-version:** fixed plugin formatting ([dd9deb9](https://gitlab.com/alexwine/aws-cron-generator/commit/dd9deb99ec8ef1d653126428d2e8f9ab353c88f8))
* added semantic-release/npm package ([9995f05](https://gitlab.com/alexwine/aws-cron-generator/commit/9995f05f90a1574064341da93e1d95fe54093e68))
