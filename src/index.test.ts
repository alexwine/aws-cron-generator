import cronTime from ".";

// describe("index.ts", () => {
//   test("should return everyDayAt", () => {
//     const res = cronTime.everyDayAt(1, 2);
//     console.log(res);

//     expect(res).toBeTruthy();
//   });
// });

test('everyMinute(): "* * * * * *"', () => {
  expect(cronTime.everyMinute()).toBe("* * * * * *");
});

test('everyHour(): "0 * * * * *"', () => {
  expect(cronTime.everyHour()).toBe("0 * * * * *");
});

test('everyHourAt(30): "30 * * * * *"', () => {
  expect(cronTime.everyHourAt(30)).toBe("30 * * * * *");
});

test('everyDay(): "0 0 * * * *"', () => {
  expect(cronTime.everyDay()).toBe("0 0 * * * *");
});

describe("every(nth)", () => {
  test('should every(5).minutes(): "*/5 * * * * *"', () => {
    expect(cronTime.every(5).minutes()).toBe("*/5 * * * * *");
  });
  test('every(5).hours(): "0 */5 * * * *"', () => {
    expect(cronTime.every(5).hours()).toBe("0 */5 * * * *");
  });
});

test(`onSpecificDays(['sunday', 'tuesday', 'thursday']): "0 0 * * 0,2,4 *"`, () => {
  expect(cronTime.onSpecificDays(["sunday", "tuesday", "thursday"])).toBe(
    "0 0 ? * 0,2,4 *"
  );
});

test(`onSpecificDaysAt(['sunday', 'tuesday', 'thursday'], 3, 30): "30 3 * * 0,2,4 *"`, () => {
  expect(
    cronTime.onSpecificDaysAt(["sunday", "tuesday", "thursday"], 3, 30)
  ).toBe("30 3 ? * 0,2,4 *");
});
